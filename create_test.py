#!/usr/bin/env python3
# coding=utf-8

num_tooltips = 1000
with open("test.html", "w") as fo:
    with open("input.html", "r") as fi:
        fo.write(fi.read())
    for i in range(1, num_tooltips):
        fo.write('<a class="tooltip" href="file1.html" data-tippy-content="<div class=\'ttc\' id=\'afile1_html\'><div class=\'ttname\'>Text of the <b>tooltip {0}</b></div></div>">other file {0}</a><br/>\n'.format(i))
    fo.write('</body></html>\n')
