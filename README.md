Tippy.js performance test
=========================

This repo contains files to test the performance of the Tippy.js plugin

| file                             | description                                            |
|----------------------------------|--------------------------------------------------------|
| README.md                        | this file                                              |
| create_test.py                   | python script to create a test file with 1000 tooltips |
| input.html                       | header part of the page used by create_test.py         |
| test-tippyjs.html                | output file to test performance using Tippy.js         |

Homepage: https://gitlab.com/simevo/tippyjs-perf-test

Based on `powertip-perf-test.zip` from https://github.com/stevenbenner/jquery-powertip/issues/178

# Install

```
sudo apt install python3 yarnpkg chromium
yarnpkg install # this will create node_modules dir
```

# Run test

```
./create_test.py # this will create test.html file
chromium test.html
```
